defmodule Shipmypart.ShipmentControllerTest do
  use Shipmypart.ConnCase

  alias Shipmypart.Shipment
  @valid_attrs %{}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, v1_shipment_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    shipment = Repo.insert! %Shipment{}
    conn = get conn, v1_shipment_path(conn, :show, shipment)
    assert json_response(conn, 200)["data"] == %{"id" => shipment.id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, v1_shipment_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, v1_shipment_path(conn, :create), shipment: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Shipment, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, v1_shipment_path(conn, :create), shipment: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    shipment = Repo.insert! %Shipment{}
    conn = put conn, v1_shipment_path(conn, :update, shipment), shipment: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Shipment, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    shipment = Repo.insert! %Shipment{}
    conn = put conn, v1_shipment_path(conn, :update, shipment), shipment: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    shipment = Repo.insert! %Shipment{}
    conn = delete conn, v1_shipment_path(conn, :delete, shipment)
    assert response(conn, 204)
    refute Repo.get(Shipment, shipment.id)
  end
end
