defmodule Shipmypart.RateControllerTest do
  use Shipmypart.ConnCase

  alias Shipmypart.Rate
  @valid_attrs %{}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, v1_rate_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    rate = Repo.insert! %Rate{}
    conn = get conn, v1_rate_path(conn, :show, rate)
    assert json_response(conn, 200)["data"] == %{"id" => rate.id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, v1_rate_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, v1_rate_path(conn, :create), rate: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Rate, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, v1_rate_path(conn, :create), rate: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    rate = Repo.insert! %Rate{}
    conn = put conn, v1_rate_path(conn, :update, rate), rate: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Rate, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    rate = Repo.insert! %Rate{}
    conn = put conn, v1_rate_path(conn, :update, rate), rate: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    rate = Repo.insert! %Rate{}
    conn = delete conn, v1_rate_path(conn, :delete, rate)
    assert response(conn, 204)
    refute Repo.get(Rate, rate.id)
  end
end
