defmodule Shipmypart.RegistrationControllerTest do
  use Shipmypart.ConnCase

  alias Shipmypart.Registration
  @valid_attrs %{}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, v1_registration_path(conn, :create), registration: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Registration, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, v1_registration_path(conn, :create), registration: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    registration = Repo.insert! %Registration{}
    conn = delete conn, v1_auth_registration_path(conn, :delete, registration)
    assert response(conn, 204)
    refute Repo.get(Registration, registration.id)
  end
end
