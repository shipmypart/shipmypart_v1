defmodule Shipmypart.SessionControllerTest do
  use Shipmypart.ConnCase

  alias Shipmypart.Auth
  @valid_attrs %{}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, v1_auth_session_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, v1_auth_session_path(conn, :create), session: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(User, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, v1_auth_session_path(conn, :create), session: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    session = Repo.insert! %User{}
    conn = delete conn, v1_auth_session_path(conn, :delete, session)
    assert response(conn, 204)
    refute Repo.get(User, session.id)
  end
end
