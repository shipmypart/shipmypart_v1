defmodule Shipmypart.ActivationControllerTest do
  use Shipmypart.ConnCase

  # alias Shipmypart.Activation
  # @valid_attrs %{}
  # @invalid_attrs %{}

  # setup %{conn: conn} do
  #   {:ok, conn: put_req_header(conn, "accept", "application/json")}
  # end

  # test "lists all entries on index", %{conn: conn} do
  #   conn = get conn, v1_auth_activation_path(conn, :index)
  #   assert json_response(conn, 200)["data"] == []
  # end

  # test "shows chosen resource", %{conn: conn} do
  #   activation = Repo.insert! %Activation{}
  #   conn = get conn, v1_auth_activation_path(conn, :show, activation)
  #   assert json_response(conn, 200)["data"] == %{"id" => activation.id}
  # end

  # test "renders page not found when id is nonexistent", %{conn: conn} do
  #   assert_error_sent 404, fn ->
  #     get conn, v1_auth_activation_path(conn, :show, -1)
  #   end
  # end

  # test "creates and renders resource when data is valid", %{conn: conn} do
  #   conn = post conn, v1_auth_activation_path(conn, :create), activation: @valid_attrs
  #   assert json_response(conn, 201)["data"]["id"]
  #   assert Repo.get_by(Activation, @valid_attrs)
  # end

  # test "does not create resource and renders errors when data is invalid", %{conn: conn} do
  #   conn = post conn, v1_auth_activation_path(conn, :create), activation: @invalid_attrs
  #   assert json_response(conn, 422)["errors"] != %{}
  # end

  # test "updates and renders chosen resource when data is valid", %{conn: conn} do
  #   activation = Repo.insert! %Activation{}
  #   conn = put conn, v1_auth_activation_path(conn, :update, activation), activation: @valid_attrs
  #   assert json_response(conn, 200)["data"]["id"]
  #   assert Repo.get_by(Activation, @valid_attrs)
  # end

  # test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
  #   activation = Repo.insert! %Activation{}
  #   conn = put conn, v1_auth_activation_path(conn, :update, activation), activation: @invalid_attrs
  #   assert json_response(conn, 422)["errors"] != %{}
  # end

  # test "deletes chosen resource", %{conn: conn} do
  #   activation = Repo.insert! %Activation{}
  #   conn = delete conn, v1_auth_activation_path(conn, :delete, activation)
  #   assert response(conn, 204)
  #   refute Repo.get(Activation, activation.id)
  # end
end
