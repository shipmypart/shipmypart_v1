defmodule Shipmypart.PaymentTokenControllerTest do
  use Shipmypart.ConnCase

  alias Shipmypart.PaymentToken
  @valid_attrs %{}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, v1_payment_token_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    payment_token = Repo.insert! %PaymentToken{}
    conn = get conn, v1_payment_token_path(conn, :show, payment_token)
    assert json_response(conn, 200)["data"] == %{"id" => payment_token.id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, v1_payment_token_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, v1_payment_token_path(conn, :create), payment_token: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(PaymentToken, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, v1_payment_token_path(conn, :create), payment_token: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    payment_token = Repo.insert! %PaymentToken{}
    conn = put conn, v1_payment_token_path(conn, :update, payment_token), payment_token: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(PaymentToken, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    payment_token = Repo.insert! %PaymentToken{}
    conn = put conn, v1_payment_token_path(conn, :update, payment_token), payment_token: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    payment_token = Repo.insert! %PaymentToken{}
    conn = delete conn, v1_payment_token_path(conn, :delete, payment_token)
    assert response(conn, 204)
    refute Repo.get(PaymentToken, payment_token.id)
  end
end
