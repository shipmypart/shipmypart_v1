defmodule Shipmypart.PaymentMethodTest do
  use Shipmypart.ModelCase

  alias Shipmypart.PaymentMethod

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = PaymentMethod.changeset(%PaymentMethod{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = PaymentMethod.changeset(%PaymentMethod{}, @invalid_attrs)
    refute changeset.valid?
  end
end
