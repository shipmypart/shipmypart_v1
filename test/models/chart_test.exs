defmodule Shipmypart.ChartTest do
  use Shipmypart.ModelCase

  alias Shipmypart.Chart

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Chart.changeset(%Chart{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Chart.changeset(%Chart{}, @invalid_attrs)
    refute changeset.valid?
  end
end
