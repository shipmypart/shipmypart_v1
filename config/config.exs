# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :shipmypart,
  ecto_repos: [Shipmypart.Repo]

# Configures the endpoint
config :shipmypart, Shipmypart.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "9sEoh7qNuX93F+u15QD37NC7e+9SQeQsPMhD3oPFqnx8M6ic1RLIO4/YgtQR0WXh",
  render_errors: [view: Shipmypart.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Shipmypart.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configure Guardian
config :guardian, Guardian,
  allowed_algos: ["ES512"],
  verify_module: Guardian.JWT,
  issuer: "localhost",
  ttl: { 7, :days},
  verify_issuer: true,
  serializer: Shipmypart.GuardianSerializer,
  secret_key: %{
    "crv" => "P-521",
    "d" => "hUwpc4bQI0xZhVVTu_Xe06ATh5pYNWSpbvlqMYaaht_PtB42WSZDVNALl2UrG0ApEk0lwrYLjbHWXJkgNceDL7Q",
    "kty" => "EC",
    "x" => "AZi26GyJr-p4OdWUuzURc1X1KRQFTHp9qYNdTRXUYt4cqCHX5tswTzrGdBcxcm45SaJOEdSs2LaSGAq2_DdZJHvV",
    "y" => "AYqFW2wmlq67scNxadEAOj3lnPxEmUk9VODaqwnCnZst4W-Arajzqgc3IXbOAJOrFwH7vMy8q_8jlVv2PBN5Kxph"
  }

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
