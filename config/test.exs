use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :shipmypart, Shipmypart.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Easypost
config :shipmypart, easypost_endpoint: "https://api.easypost.com/v2/",
                    easypost_key: ""

# SendGrid
config :shipmypart, Shipmypart.Mailer,
  adapter: Bamboo.LocalAdapter

# turn bcrypt down for speed during tests
config :comeonin, :bcrypt_log_rounds, 4
config :comeonin, :pbkdf2_rounds, 1

# Configure your database
config :shipmypart, Shipmypart.Repo,
  adapter: Ecto.Adapters.Postgres,
  # username: "postgres",
  # password: "postgres",
  database: "shipmypart_test",
  hostname: System.get_env("DB_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
