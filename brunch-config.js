exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {
      joinTo: 'src/index.js'
    },
    stylesheets: {
      joinTo: 'src/index.css'
    },
    templates: {
      joinTo: 'src/index.js'
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to "/web/static/assets". Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(web\/static\/assets)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: [
      'web/static',
      'test/static'
    ],

    // Where to compile files to
    public: 'priv/static'
  },

  // Configure your plugins
  plugins: {
    babel: {
      ignore: [/web\/static\/vendor/], // Do not use ES6 compiler in vendor code
      presets: [
        'es2015'
      ]
    },
    sass: {
      mode: 'native',
      precision: 8,
      sourceMapEmbed: true
    },
    postcss: {
      processors: [
        require('postcss-url')({
          url: 'inline'
        }),
        require('autoprefixer')(['defaults', '> 3%']),
        require('csswring')()
      ]
    },
    uglify: {
      mangle: true
    }
  },

  modules: {
    autoRequire: {
      'src/index.js': [
        'jquery',
        'web/static/src/index'
      ]
    }
  },

  npm: {
    enabled: true,
    globals: {
      $: 'jquery',
      jQuery: 'jquery'
    },
    whitelist: [
      'phoenix',
      'phoenix_html',
      'jquery',
      'vue',
      'vuex',
      'vue-router',
      'axios',
      'localforage',
      'lodash',
      'moment-timezone'
    ]
  }
}
