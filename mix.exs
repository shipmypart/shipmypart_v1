defmodule Shipmypart.Mixfile do
  use Mix.Project

  def project do
    [app: :shipmypart,
     version: "0.0.1",
     elixir: "~> 1.3",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases(),
     deps: deps(),
     dialyzer: [plt_add_deps: :transitive],
     test_coverage: [tool: ExCoveralls],
     preferred_cli_env: [coveralls: :test]]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {Shipmypart, []},
     applications: [:logger, :phoenix, :phoenix_pubsub, :phoenix_ecto,
                    :postgrex, :phoenix_html, :gettext, :cowboy,
                    :comeonin, :bamboo, :braintree, :timex_ecto
                    ]]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [{:phoenix, "~> 1.2"},
     {:phoenix_pubsub, "~> 1.0"},
     {:phoenix_ecto, "~> 3.0"}, # >= 3.1 breaks poison dependency resolution. :(
     {:postgrex, ">= 0.0.0"},
     {:phoenix_html, "~> 2.9"},
     {:phoenix_live_reload, "~> 1.0", only: [:dev]},
     {:ex_doc, "~> 0.14.5", only: [:dev], runtime: false},
     {:dialyxir, "~> 0.4", only: [:dev], runtime: false},
     {:credo, "~> 0.6", only: [:dev, :test], runtime: false},
     {:excoveralls, "~> 0.6", only: [:dev, :test]},
     {:gettext, "~> 0.13"},
     {:cowboy, "~> 1.0"},
     {:comeonin, "~> 3.0"},
     {:guardian, "~> 0.14"},
     {:bamboo, "~> 0.8"},
     {:braintree, "~> 0.7"},
     {:easypost, "~> 0.0.1"},
     {:timex, "~> 3.1"},
     {:timex_ecto, "~> 3.0"}]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs", "run priv/repo/seeds_zone.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"],
     "test": ["ecto.create --quiet", "ecto.migrate", "test"]]
  end
end
