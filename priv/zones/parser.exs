
file = "./priv/zones/current/Format2.txt"
|> File.stream! # Import the file
|> Stream.map(&String.strip/1)
|> Enum.to_list

{effective, zones} = List.pop_at(file, 0)

effective
|> Timex.parse!("%m%d%Y", :strftime)
|> IO.inspect

zones
|> Enum.take(2)
|> IO.inspect
