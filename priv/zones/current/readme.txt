EXCEPTIONS FOR 04/01/2014                                                       
                                                                                
The following Origin/Destination 5-Digit ZIP Code ranges will be                
assigned to Zone 8:                                                             
                                                                                
Origin      Destination                                                         
00100-96199 96900-96938                                                         
00100-96199 96945-96959                                                         
00100-96199 96961-96969                                                         
00100-96199 96971-96999                                                         
97000-99999 96900-96938                                                         
97000-99999 96945-96959                                                         
97000-99999 96961-96969                                                         
97000-99999 96971-96999                                                         
                                                                                
NOTE: Mail originating from 962 through 969 to the 969 ZIP Codes are            
not affected. Use the Zone Chart Matrix assignment for those O/D pairs.         
                                                                                
Defining a Zone                                                                 
                                                                                
*Local Zone - The local zone applies to mail deposited at any post              
 office for delivery to any address within the delivery area of that            
 post office. Check with your local post office for specific details            
 regarding the ZIP Codes that qualify for local zone in your area. The          
 Zone Chart does not contain local zone information since local zones           
 are at the 5-digit ZIP Code level.                                             
*Non-Local Zone - Non-local zones are defined numerically as follows.           
Zone   Distance                                                                 
1      Non-local zones within a 50 mile radius of the point of origination      
2      51 to 150 mile radius                                                    
3      151 to 300 mile radius                                                   
4      301 to 600 mile radius                                                   
5      601 to 1000 mile radius                                                  
6      1001 to 1400 mile radius                                                 
7      1401 to 1800 mile radius                                                 
8      1801 miles and over                                                      
9*     ZIP Codes Assigned For Exceptional Network Circumstances **              
*  Except for Priority Mail and Priority Mail Express mailed TO the             
   Zone 9 ZIP Codes, Zone 9 prices will be the same as Zone 8 prices            
** Shipping between the 3-digit ZIP Codes 967, 968 or 969 does not apply        
                                                                                
CURRENT ZONE CHART MATRIX FILE STRUCTURE                                        
                                                                                
Description                  Start    End       Field  Acceptable               
                             Position Position  Length Values                   
Originating ZIP Code         1        3         3      Numeric                  
Zone to Destination: ZIP 001 4        4         1      Numeric                  
Filler: ZIP 001              5        5         1      * e b or space           
Zone to Destination: ZIP 002 6        6         1      Numeric                  
Filler: ZIP 002              7        7         1      * e b or space           
Zone to Destination: ZIP 003 8        8         1      NUMERIC                  
Filler: ZIP 003              9        9         1      * e b or space           
This format continues for each 3-digit ZIP to the end of the 3 Digits           
matrix file.                                                                    
Zone to Destination: ZIP 969 1940  1940         1      Numeric                  
Filler: ZIP 969              1941  1941         1      * e b or space           
(etc)                                                                           
Zone to Destination: ZIP 998 1998  1998         1      Numeric                  
Filler: ZIP 998              1999  1999         1      * e b or space           
Zone to Destination: ZIP 999 2000  2000         1      Numeric                  
Filler: ZIP 999              2001  2001         1      * e b or space           
Carriage Return Line Feed    2002  2003         2                               
* = NDC Entry Discount indicator                                                
e = 5-Digit ZIP Code Exception indicator - check Exceptions File                
b = Both 5-Digit ZIP Code Exception and NDC Entry Discount indicator -check     
    Exceptions File                                                             
                                                                                
EXCEPTIONS FILE STRUCTURE                                                       
                                                                                
Description                 Start    End      Field  Acceptable                 
                            Position Position Length Values                     
Origin ZIP Code Range START 1        5        5      3 or 5 Numeric             
Origin ZIP Code Range END   6        10       5      3 or 5 Numeric             
Dest ZIP Code Range START   11       15       5      3 or 5 Numeric             
Dest ZIP Code Range END     16       20       5      3 or 5 Numeric             
Zone                        21       22       2   Numeric (leading zero)        
Filler                      23       30       8   Alpha/Numeric/Space           
Carriage Return             31       31       1                                 
