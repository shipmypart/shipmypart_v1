alias Shipmypart.Repo
alias ShipMyPart.Zone

# Rate Zones
# %{origin: "", dest: "", zone: 0, discounted: false, effective: ""},
Repo.insert_all!(Zone, [
])

defmodule Shipmypart.Zonefile.Parser do
  # Interface
  def parse(filepath) do
    filepath
    |> File.stream!                 # Import the file
    |> Stream.map(&String.strip/1)  # Map the lines to a string
    |> Enum.to_list                 # Put all the lines in a list
    |> parse_lines
  end

  # Private
  defp parse_lines(lines) do
    {effective, zones} = Line.pop_at(0)
    
  end
end

Shipmypart.Zonefile.Parser.parse("./priv/zones/current/Format2.txt")
