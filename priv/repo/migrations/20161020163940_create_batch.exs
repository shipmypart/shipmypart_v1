defmodule Shipmypart.Repo.Migrations.CreateBatch do
  use Ecto.Migration

  def change do
    create table(:batches, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string
      add :status, :string
      add :state, :string
      add :easypost_id, :string
      
      # Relationships
      add :company_id, references(:companies, type: :uuid)

      timestamps()
    end
    create index(:batches, [:company_id])
    create index(:batches, [:easypost_id])
  end
end
