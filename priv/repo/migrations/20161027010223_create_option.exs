defmodule Shipmypart.Repo.Migrations.CreateOption do
  use Ecto.Migration

  def change do
    create table(:options, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :timezone, :string, default: "Etc/UTC"
      add :label_type, :string, default: "PNG"

      # Relationships
      add :user_id, references(:users, type: :uuid)

      timestamps()
    end
    create index(:options, [:user_id])

  end
end
