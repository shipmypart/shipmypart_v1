defmodule Shipmypart.Repo.Migrations.CreateZone do
  use Ecto.Migration

  def change do
    create table(:zones, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :origin, :string
      add :dest, :string
      add :zone, :integer
      add :discounted, :boolean
      add :effective, :datetime

      timestamps()
    end
    create index(:zones, [:origin, :dest], unique: true)

  end
end
