defmodule Shipmypart.Repo.Migrations.CreateRate do
  use Ecto.Migration

  def change do
    create table(:rates, primary_key: false) do
      add :id, :uuid, primary_key: true
      
      timestamps()
    end

  end
end
