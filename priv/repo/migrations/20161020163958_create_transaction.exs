defmodule Shipmypart.Repo.Migrations.CreateTransaction do
  use Ecto.Migration

  def change do
    create table(:transactions, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :description, :string, null: false
      add :credit, :float
      add :debit, :float
      add :balance, :float, null: false

      # Relationships
      add :company_id, references(:companies, type: :uuid)

      timestamps()
    end
    create index(:transactions, [:company_id])

  end
end