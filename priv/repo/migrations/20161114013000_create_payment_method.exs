defmodule Shipmypart.Repo.Migrations.CreatePaymentMethod do
  use Ecto.Migration

  def change do
    create table(:paymentmethods, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :card_type, :string
      add :cardholder_name, :string
      add :customer_id, :string
      add :default, :boolean
      add :expiration_month, :string
      add :expiration_year, :string
      add :expired, :boolean
      add :image_url, :string
      add :last_4, :string
      add :token, :string
      add :unique_number_identifier, :string
      add :verifications, {:array, :map}

      # Relationships
      add :company_id, references(:companies, type: :uuid)

      timestamps()
    end
    create index(:paymentmethods, [:company_id])

  end
end
