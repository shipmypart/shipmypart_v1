defmodule Shipmypart.Repo.Migrations.CreateShipment do
  use Ecto.Migration

  def change do
    create table(:shipments, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :parcel, :map
      add :customs_info, :map
      add :tracking_code, :string
      add :insurance, :float
      add :reference, :string
      add :carrier, :string
      add :service, :string
      add :rate, :float
      add :label_url, :string
      add :easypost_id, :string
      add :purchased, :boolean, default: false
      add :refunded, :boolean, default: false
      add :in_transit, :boolean, default: false
      add :delivered, :boolean, default: false

      # Relationships
      add :chart_id, references(:charts, type: :uuid)
      add :batch_id, references(:batches, type: :uuid)
      add :company_id, references(:companies, type: :uuid)
      add :to_address_id, references(:addresses, type: :uuid)
      add :transaction_id, references(:transactions, type: :uuid)
      add :from_address_id, references(:addresses, type: :uuid)    

      timestamps()
    end
    create index(:shipments, [:chart_id])
    create index(:shipments, [:batch_id])
    create index(:shipments, [:company_id])
    create index(:shipments, [:easypost_id])
    create index(:shipments, [:to_address_id])
    create index(:shipments, [:transaction_id])
    create index(:shipments, [:from_address_id])
    
  end
end
