defmodule Shipmypart.Repo.Migrations.CreateAddress do
  use Ecto.Migration

  def change do
    create table(:addresses, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string
      add :company, :string
      add :street1, :string
      add :street2, :string
      add :city, :string
      add :state, :string
      add :zip, :string
      add :country, :string
      add :phone, :string
      add :email, :string
      add :easypost_id, :string
      
      # Relationships
      add :company_id, references(:companies, type: :uuid)

      timestamps()
    end
    create index(:addresses, [:company_id])
    create index(:addresses, [:easypost_id])
  end
end
