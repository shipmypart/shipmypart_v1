defmodule Shipmypart.Repo.Migrations.CreateLocation do
  use Ecto.Migration

  def change do
    create table(:locations, primary_key: false) do
      add :id, :uuid, primary_key: true

      # Relationships
      add :company_id, references(:companies, type: :uuid)

      timestamps()
    end
    create index(:locations, [:company_id])

  end
end
