defmodule Shipmypart.Repo.Migrations.CreateChart do
  use Ecto.Migration

  def change do
    create table(:charts, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :zone1, {:array, :string}
      add :zone2, {:array, :string}
      add :zone3, {:array, :string}
      add :zone4, {:array, :string}
      add :zone5, {:array, :string}
      add :zone6, {:array, :string}
      add :zone7, {:array, :string}
      add :zone8, {:array, :string}
      add :zone9, {:array, :string}
      add :service, :string
      add :expires, :datetime
      add :available, :datetime

      # Relationships
      add :company_id, references(:companies, type: :uuid)

      timestamps()
    end

  end
end
