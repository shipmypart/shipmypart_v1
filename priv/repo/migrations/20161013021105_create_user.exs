defmodule Shipmypart.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :first_name, :string, null: false
      add :last_name, :string, null: false
      add :email, :string, null: false
      add :encrypted_password, :string, null: false
      add :is_active, :boolean
      add :activation_token, :string, null: false
      add :activation_token_expiration, :datetime
      add :admin, :boolean
      add :customer_id, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
