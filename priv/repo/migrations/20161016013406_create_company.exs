defmodule Shipmypart.Repo.Migrations.CreateCompany do
  use Ecto.Migration

  def change do
    create table(:companies, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string
      add :is_active, :boolean
      add :is_past_due, :boolean

      # Relationships
      add :user_id, references(:users, type: :uuid)

      timestamps()
    end
    create index(:companies, [:user_id])
  end
end
