defmodule Shipmypart.Repo.Migrations.CreatePayment do
  use Ecto.Migration

  def change do
    create table(:payments, primary_key: false) do
      add :id, :uuid, primary_key: true

      # Relationships
      add :company_id, references(:companies, type: :uuid)

      timestamps()
    end
    create index(:payments, [:company_id])

  end
end
