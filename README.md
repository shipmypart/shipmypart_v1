# Shipmypart  [![build status](https://gitlab.com/shipmypart/shipmypart/badges/master/build.svg)](https://gitlab.com/shipmypart/shipmypart/commits/master)

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

## Heroku
```
# Create a Heroku instance for your project
heroku apps:create my_heroku_app

# Set and add the buildpacks for your Heroku app
heroku buildpacks:set https://github.com/HashNuke/heroku-buildpack-elixir
heroku buildpacks:add https://github.com/gjaldon/heroku-buildpack-phoenix-static

# Deploy
git push heroku master
```

# Generating Secrets
### For Phoenix (SECRET_KEY_BASE)
```
mix phoenix.gen.secret
```

### For JWT
```
iex -S mix
jwk = JOSE.JWK.generate_key({:ec, "P-521"}) |> JOSE.JWK.to_map |> elem(1)
> %{"crv" => "P-521",
  "d" => "VS1tRaj--a3Kiv5r...",
  "kty" => "EC",
  "x" => "ALj0LWUKQh_4OkoV...",
  "y" => "ANalVpk1c9_ttHxx..."}
```

# Enviroment Variables
### For production
```
DATABASE_URL
DOMAIN
SECRET_KEY_BASE
SECRET_KEY_D
SECRET_KEY_X
SECRET_KEY_Y
EASYPOST_ENDPOINT
EASYPOST_KEY
SENDGRID_KEY
BRAINTREE_MERCHANT_ID
BRAINTREE_PUBLIC_KEY
BRAINTREE_PRIVATE_KEY
LE_CHALLENGE_KEY
ANALYTICS_KEY
```

# Emails
### For development

We use [Bamboo](github.com/thoughtbot/bamboo) for emails, because it allows us to easily switch between various providers.
However, for **local** development, all sent emails are saved and never actually sent.

So there is a special endpoint available to view any sent emails.
Browse to: `localhost:4000/sent_emails` to see all the emails the application has emitted.
