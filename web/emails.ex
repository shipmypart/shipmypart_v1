defmodule Shipmypart.Email do
  use Bamboo.Phoenix, view: Shipmypart.EmailView

  def welcome_email(conn, user) do
    base_email(conn)
    |> assign(:user, user)
    |> to(user.email)
    |> subject("Welcome to ShipMyPart!")
    |> render(:welcome)
  end

  defp base_email(conn) do
    new_email()
    |> assign(:conn, conn)
    |> from({"ShipMyPart Robot", "robot@shipmypart.com"})
    |> put_header("Reply-To", "support@shipmypart.com")
    |> put_html_layout({Shipmypart.LayoutView, "email.html"})
  end
end
