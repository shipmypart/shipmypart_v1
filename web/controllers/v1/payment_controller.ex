defmodule Shipmypart.V1.PaymentController do
  use Shipmypart.Web, :controller

  # alias Braintree.ClientToken
  # alias Braintree.ErrorResponse, as: Error

  alias Shipmypart.Payment

  def action(conn, _) do
    apply(__MODULE__, action_name(conn),
          [conn, conn.params, conn.assigns.current_user])
  end

  # Return all payment methods 
  def index(conn, _params, _current_user) do
    payments = Repo.all(Payment)
    render(conn, "index.json", payments: payments)
  end

  # Retrieve a single payment
  def show(conn, %{"id" => id}, _current_user) do
    payment = Repo.get!(Payment, id)
    render(conn, "show.json", payment: payment)
  end

  # Add a new payment method
  def create(conn, %{"payment" => payment_params}, _current_user) do
    changeset = Payment.changeset(%Payment{}, payment_params)

    case Repo.insert(changeset) do
      {:ok, payment} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", v1_payment_path(conn, :show, payment))
        |> render("show.json", payment: payment)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  # ???
  def update(conn, %{"id" => id, "payment" => payment_params}, _current_user) do
    payment = Repo.get!(Payment, id)
    changeset = Payment.changeset(payment, payment_params)

    case Repo.update(changeset) do
      {:ok, payment} ->
        render(conn, "show.json", payment: payment)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  # Remove a payment method
  def delete(conn, %{"id" => id}, _current_user) do
    payment = Repo.get!(Payment, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(payment)

    send_resp(conn, :no_content, "")
  end
end
