defmodule Shipmypart.V1.Auth.SessionController do
  use Shipmypart.Web, :controller

  alias Shipmypart.Auth
  
  def create(conn, %{"username" => email, "password" => password}) do
    case Auth.basic_login(email, password, repo: Repo) do
      {:ok, user} ->
        new_conn = Guardian.Plug.api_sign_in(conn, user, :jwt)
        jwt = Guardian.Plug.current_token(new_conn)
        {:ok, claims} = Guardian.Plug.claims(new_conn)
        exp = Map.get(claims, "exp")
        
        new_conn
        |> put_resp_header("authorization", "Bearer #{jwt}")
        |> put_resp_header("x-expires", "#{exp}")
        |> render("show.json", session: %{user: user, jwt: jwt, exp: exp})
      {:error, reason} ->
        conn
        |> put_status(reason)
        |> render("error.json", message: get_reason(reason))
    end
  end

  def delete(conn, _params) do 
    # TODO: we require GuardianDB to enable the next three lines
    # jwt = Guardian.Plug.current_token(conn)
    # claims = Guardian.Plug.claims(conn)
    # Guardian.revoke!(jwt, claims)
    
    send_resp(conn, :no_content, "")
  end

  defp get_reason(reason) do
    cond do
      reason == :not_found ->
        "User not found"
      reason == :unauthorized ->
        "Invalid login"
      reason == :unprocessable_entity ->
        "Account is not active"
    end
  end
end
