defmodule Shipmypart.V1.Auth.ActivationController do
  use Shipmypart.Web, :controller

  alias Shipmypart.User

  def show(conn, %{"id" => id}) do
    activation = User
      |> where(activation_token: ^id)
      |> where([record], record.activation_token_expiration >= ^Timex.now)
      |> Repo.one!
    render(conn, "show.json", activation: activation)
  end

  def update(conn, %{"id" => id, "activation" => activation_params}) do
    changeset = User 
      |> where(activation_token: ^id)
      |> Repo.one!
      |> User.activation_changeset(activation_params)

    case Repo.update(changeset) do
      {:ok, activation} ->
        render(conn, "show.json", activation: activation)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end
end
