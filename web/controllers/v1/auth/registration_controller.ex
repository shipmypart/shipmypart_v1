defmodule Shipmypart.V1.Auth.RegistrationController do
  use Shipmypart.Web, :controller

  alias Shipmypart.{User, Email, Mailer, Transaction}

  def create(conn, %{"user" => user_params}) do
    changeset = User.registration_changeset(%User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, registration} ->
        # Create initial transaction
        registration.company
        |> Transaction.new

        # Send email
        Email.welcome_email(conn, registration)
        |> Mailer.deliver_later

        # Return response
        conn
        |> put_status(:created)
        |> put_resp_header("location", v1_auth_registration_path(conn, :show, registration))
        |> render("show.json", registration: registration)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    registration = Repo.get!(User, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(registration)

    send_resp(conn, :no_content, "")
  end
end
