defmodule Shipmypart.V1.AddressController do
  use Shipmypart.Web, :controller
  use Easypost.Client, endpoint: Application.get_env(:shipmypart, :easypost_endpoint),                    
                           key: Application.get_env(:shipmypart, :easypost_key)

  alias Shipmypart.Address

  def action(conn, _) do
    if (conn.assigns.current_user.company == nil) do
      Phoenix.Controller.json conn
      |> put_status(:unprocessable_entity), %{errors: "User must be assoicated with a company before using the API"}
    end
    apply(__MODULE__, action_name(conn),
        [conn, conn.params, conn.assigns.current_user])
  end

  def index(conn, _params, current_user) do
    addresses = Address
              |> where(company_id: ^current_user.company.id)
              |> Repo.all

    render(conn, "index.json", addresses: addresses)
  end

  def show(conn, %{"id" => id}, current_user) do
    address = Address
              |> where(company_id: ^current_user.company.id)
              |> Repo.get!(id)

    render(conn, "show.json", address: address)
  end

  def create(conn, %{"address" => address_params}, current_user) do
    changeset = Address.associate_company(%Address{}, address_params, current_user)

    case Repo.insert(changeset) do
      {:ok, address} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", v1_address_path(conn, :show, address))
        |> render("show.json", address: address)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "address" => address_params}, current_user) do
    address = Address
              |> where(company_id: ^current_user.company.id)
              |> Repo.get!(id)
              
    changeset = Address.changeset(address, address_params)

    case Repo.update(changeset) do
      {:ok, address} ->
        render(conn, "show.json", address: address)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, current_user) do
    address = Address
              |> where(company_id: ^current_user.company.id)
              |> Repo.get!(id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(address)

    send_resp(conn, :no_content, "")
  end
end
