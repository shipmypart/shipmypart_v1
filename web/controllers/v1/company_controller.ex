defmodule Shipmypart.V1.CompanyController do
  use Shipmypart.Web, :controller

  alias Shipmypart.Company

  def action(conn, _) do
    apply(__MODULE__, action_name(conn),
          [conn, conn.params, conn.assigns.current_user])
  end

  def index(conn, _params, current_user) do
    companies = Company
                |> where(id: ^current_user.company.id)
                |> Repo.all
                
    render(conn, "index.json", companies: companies)
  end

  def show(conn, %{"id" => id}, current_user) do
    company = Company
              |> Repo.get!(current_user.company.id)

    render(conn, "show.json", company: company)
  end

  def update(conn, %{"company" => company_params}, current_user) do
    company = Company
              |> Repo.get!(current_user.company.id)

    changeset = Company.changeset(company, company_params)

    case Repo.update(changeset) do
      {:ok, company} ->
        render(conn, "show.json", company: company)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, _params, current_user) do
    company = Company
              |> Repo.get!(current_user.company.id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(company)

    send_resp(conn, :no_content, "")
  end
end
