defmodule Shipmypart.V1.PaymentMethodController do
  use Shipmypart.Web, :controller

  # alias Braintree.ClientToken
  alias Braintree.ErrorResponse, as: Error
  alias Shipmypart.PaymentMethod

  def action(conn, _) do
    if (conn.assigns.current_user.company == nil) do
      Phoenix.Controller.json conn
      |> put_status(:unprocessable_entity), %{errors: "User must be assoicated with a company before using the API"}
    end
    apply(__MODULE__, action_name(conn),
          [conn, conn.params, conn.assigns.current_user])
  end

  def index(conn, _params, current_user) do
    paymentmethods = PaymentMethod
                     |> where(company_id: ^current_user.company.id)
                     |> Repo.all

    render(conn, "index.json", paymentmethods: paymentmethods)
  end

  def show(conn, %{"id" => id}, current_user) do
    payment_method = PaymentMethod
                     |> where(company_id: ^current_user.company.id)
                     |> Repo.get!(id)

    render(conn, "show.json", payment_method: payment_method)
  end

  def create(conn, %{"payment_method" => payment_method_params}, current_user) do
    case Braintree.PaymentMethod.create(%{
      customer_id: current_user.customer_id,
      payment_method_nonce: payment_method_params["payment_method_nonce"],
      options: %{
        make_default: true,
        verify_card: true
      }
    }) do
      {:ok, payment_method_response} ->
        changeset = PaymentMethod.from_braintree(%PaymentMethod{}, payment_method_response, current_user)

        case Repo.insert(changeset) do
          {:ok, payment_method} ->
            conn
            |> put_status(:created)
            |> put_resp_header("location", v1_payment_method_path(conn, :show, payment_method))
            |> render("show.json", payment_method: payment_method)
          {:error, changeset} ->
            conn
            |> put_status(:unprocessable_entity)
            |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
        end
      {:error, %Error{} = error} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("braintree_error.json", error: error)
    end
  end

  def delete(conn, %{"id" => id}, current_user) do
    payment_method = PaymentMethod
                     |> where(company_id: ^current_user.company.id)
                     |> Repo.get!(id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(payment_method)

    send_resp(conn, :no_content, "")
  end
end
