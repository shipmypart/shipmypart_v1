defmodule Shipmypart.V1.RateController do
  use Shipmypart.Web, :controller

  alias Shipmypart.Rate
  
  def show(conn, %{"id" => id}) do
    rate = Repo.get!(Rate, id)
    render(conn, "show.json", rate: rate)
  end
end
