defmodule Shipmypart.V1.PaymentTokenController do
  use Shipmypart.Web, :controller

  alias Braintree.ClientToken
  alias Braintree.ErrorResponse, as: Error

  # Return a payment token
  def new(conn, _params) do
    case ClientToken.generate(%{version: 3}) do
      {:ok, payment_token} ->
        conn
        |> put_status(:ok)
        |> render("payment_token.json", payment_token: payment_token)
      {:error, %Error{} = error} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", changeset: error)
    end
  end
end
