defmodule Shipmypart.V1.LocationController do
  use Shipmypart.Web, :controller

  alias Shipmypart.Location

  def action(conn, _) do
    apply(__MODULE__, action_name(conn),
          [conn, conn.params, conn.assigns.current_user])
  end

  def index(conn, _params, current_user) do
    locations = Location
                |> where(company_id: ^current_user.company.id)
                |> Repo.all

    render(conn, "index.json", locations: locations)
  end

  def show(conn, %{"id" => id}, current_user) do
    location = Location
               |> where(company_id: ^current_user.company.id)
               |> Repo.get!(id)
    render(conn, "show.json", location: location)
  end

  def create(conn, %{"location" => location_params}, current_user) do
    changeset = Location.associate_company(%Location{}, location_params, current_user)

    case Repo.insert(changeset) do
      {:ok, location} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", v1_location_path(conn, :show, location))
        |> render("show.json", location: location)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "location" => location_params}, current_user) do
    location = Location
               |> where(company_id: ^current_user.company.id)
               |> Repo.get!(id)
    changeset = Location.changeset(location, location_params)

    case Repo.update(changeset) do
      {:ok, location} ->
        render(conn, "show.json", location: location)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, current_user) do
    location = Location
               |> where(company_id: ^current_user.company.id)
               |> Repo.get!(id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(location)

    send_resp(conn, :no_content, "")
  end
end
