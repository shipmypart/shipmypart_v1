defmodule Shipmypart.V1.BatchController do
  use Shipmypart.Web, :controller
  use Easypost.Client, endpoint: Application.get_env(:shipmypart, :easypost_endpoint),                    
                           key: Application.get_env(:shipmypart, :easypost_key)

  alias Shipmypart.Batch

  def action(conn, _) do
    apply(__MODULE__, action_name(conn),
          [conn, conn.params, conn.assigns.current_user])
  end

  def index(conn, _params, current_user) do
    batches = Batch
              |> where(company_id: ^current_user.company.id) 
              |> Repo.all
              |> Repo.preload([:shipments])
              
    render(conn, "index.json", batches: batches)
  end

  def show(conn, %{"id" => id}, current_user) do
    batch = Batch
            |> where(company_id: ^current_user.company.id)
            |> Repo.get!(id)

    render(conn, "show.json", batch: batch)
  end

  def create(conn, %{"batch" => batch_params}, current_user) do
    changeset = Batch.associate_company(%Batch{}, batch_params, current_user)

    case Repo.insert(changeset) do
      {:ok, batch} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", v1_batch_path(conn, :show, batch))
        |> render("show.json", batch: batch |> Repo.preload([:shipments]))
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "batch" => batch_params}, current_user) do
    batch = Batch
            |> where(company_id: ^current_user.company.id)
            |> Repo.get!(id)
            
    changeset = Batch.changeset(batch, batch_params)

    case Repo.update(changeset) do
      {:ok, batch} ->
        render(conn, "show.json", batch: batch |> Repo.preload([:shipments]))
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, current_user) do
    batch = Batch
            |> where(company_id: ^current_user.company.id)
            |> Repo.get!(id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(batch)

    send_resp(conn, :no_content, "")
  end
end
