defmodule Shipmypart.V1.ShipmentController do
  use Shipmypart.Web, :controller
  use Easypost.Client, endpoint: Application.get_env(:shipmypart, :easypost_endpoint),                    
                           key: Application.get_env(:shipmypart, :easypost_key)

  import Ecto.Changeset # TODO: once finished moving logic to model, delete this import
  import Phoenix.Controller

  # alias Shipmypart.Company
  alias Shipmypart.{Shipment, Address}

  def action(conn, _) do
    apply(__MODULE__, action_name(conn),
          [conn, conn.params, conn.assigns.current_user])
  end

  def index(conn, _params, current_user) do
    shipments = Shipment
                |> where(company_id: ^current_user.company.id)
                |> Repo.all

    render(conn, "index.json", shipments: shipments)
  end

  def show(conn, %{"id" => id}, current_user) do
    shipment = Shipment
               |> where(company_id: ^current_user.company.id)
               |> Repo.get!(id)
               |> Repo.preload([:company, :to_address, :from_address])

    render(conn, "show.json", shipment: shipment)
  end

  # TODO: move this logic to the model
  def create(conn, %{"shipment" => shipment_params}, current_user) do
    case create_shipment(shipment_params) do
      {:ok, response} ->
        to_address = Address.changeset(%Address{}, response.to_address |> Map.from_struct |> Map.put(:easypost_id, response.to_address.id)) 
          |> Repo.insert!
        from_address = Address.changeset(%Address{}, response.from_address |> Map.from_struct |> Map.put(:easypost_id, response.from_address.id)) 
          |> Repo.insert!
        changeset = Shipment.changeset(%Shipment{}, 
          shipment_params
            |> Map.put("rates", response.rates |> Enum.map(fn(x) -> Map.from_struct(x) end)) 
            |> Map.put("easypost_id", response.id))
          |> put_assoc(:company, current_user.company)
          |> put_assoc(:to_address, to_address)
          |> put_assoc(:from_address, from_address)

        case Repo.insert(changeset) do
          {:ok, shipment} ->
            conn
            |> put_status(:created)
            |> put_resp_header("location", v1_shipment_path(conn, :show, shipment))
            |> render("show.json", shipment: shipment)
          {:error, changeset} ->
            conn
            |> put_status(:unprocessable_entity)
            |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
        end
      {:error, reason} ->
        json conn
        |> put_status(:unprocessable_entity), %{"errors": reason}
    end
  end

  # TODO: move this logic to the model
  def update(conn, %{"id" => id, "rate" => rate_params}, current_user) do
    case buy_shipment(rate_params["shipment_id"], rate_params) do
      {:ok, purchased_shipment} ->
        case Shipment.easypost_purchase(current_user, id, purchased_shipment) do
          {:ok, shipment} ->
            render(conn, "show.json", shipment: shipment)
          {:error, changeset} ->
            conn
            |> put_status(:unprocessable_entity)
            |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
        end
      {:error, reason} ->
        json conn
        |> put_status(:unprocessable_entity), %{"errors": reason}
    end
  end

  # TODO: move this logic to the model and REWRITE THIS, DOESN'T WORK
  def delete(conn, %{"id" => id}, current_user) do
    shipment = Shipment
            |> where(company_id: ^current_user.company.id)
            |> Repo.get!(id)
            |> Repo.preload([:to_address, :from_address])

    case refund_usps_label(shipment.easypost_id) do
      {:ok, _response} ->
        shipment_params = shipment
                          |> Map.from_struct
                          |> Map.put(:refunded, true)
                          |> Map.put(:to_address, shipment.to_address |> Map.from_struct)
                          |> Map.put(:from_address, shipment.from_address |> Map.from_struct)
        changeset = Shipment.changeset(shipment, shipment_params)

        case Repo.update(changeset) do
          {:ok, _shipment} ->
            send_resp(conn, :no_content, "")
          {:error, changeset} ->
            conn
            |> put_status(:unprocessable_entity)
            |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
        end
      {:error, reason} ->
        json conn
        |> put_status(:unprocessable_entity), %{"errors": reason}
    end

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    # Repo.delete!(shipment)

    # send_resp(conn, :no_content, "")
  end
end
