defmodule Shipmypart.V1.Admin.ChartController do
  use Shipmypart.Web, :controller

  alias Shipmypart.Chart

  def index(conn, _params) do
    charts = Repo.all(Chart)
    render(conn, "index.json", charts: charts)
  end

  def show(conn, %{"id" => id}) do
    chart = Repo.get!(Chart, id)
    render(conn, "show.json", chart: chart)
  end

  def create(conn, %{"chart" => chart_params}) do
    changeset = Chart.changeset(%Chart{}, chart_params)

    case Repo.insert(changeset) do
      {:ok, chart} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", v1_admin_chart_path(conn, :show, chart))
        |> render("show.json", chart: chart)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "chart" => chart_params}) do
    chart = Repo.get!(Chart, id)
    changeset = Chart.changeset(chart, chart_params)

    case Repo.update(changeset) do
      {:ok, chart} ->
        render(conn, "show.json", chart: chart)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    chart = Repo.get!(Chart, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(chart)

    send_resp(conn, :no_content, "")
  end
end
