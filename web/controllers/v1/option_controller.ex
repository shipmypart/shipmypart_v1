defmodule Shipmypart.V1.OptionController do
  use Shipmypart.Web, :controller

  alias Shipmypart.Option

  def action(conn, _) do
    apply(__MODULE__, action_name(conn),
          [conn, conn.params, conn.assigns.current_user])
  end

  def index(conn, _params, current_user) do
    options = Option
              |> where(user_id: ^current_user.id)
              |> Repo.all
    render(conn, "index.json", options: options)
  end

  def show(conn, %{"id" => id}, current_user) do
    option = Option
             |> where(user_id: ^current_user.id)
             |> Repo.get!(id)
    render(conn, "show.json", option: option)
  end

  def create(conn, %{"option" => option_params}, current_user) do
    changeset = Option.associate_user(%Option{}, option_params, current_user)

    case Repo.insert(changeset) do
      {:ok, option} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", v1_option_path(conn, :show, option))
        |> render("show.json", option: option)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def update(conn, %{"id" => id, "option" => option_params}, current_user) do
    option = Option
             |> where(user_id: ^current_user.id)
             |> Repo.get!(id)
    changeset = Option.changeset(option, option_params)

    case Repo.update(changeset) do
      {:ok, option} ->
        render(conn, "show.json", option: option)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Shipmypart.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, current_user) do
    option = Option
             |> where(user_id: ^current_user.id)
             |> Repo.get!(id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(option)

    send_resp(conn, :no_content, "")
  end
end
