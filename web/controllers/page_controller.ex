defmodule Shipmypart.PageController do
  use Shipmypart.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def letsencrypt(conn, _params) do
    text conn, Application.get_env(:shipmypart, :lets_encrypt_challenge)
  end
end
