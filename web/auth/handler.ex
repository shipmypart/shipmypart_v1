defmodule Shipmypart.AuthHandler do
    def unauthenticated(conn, _params) do
        conn
        |> Plug.Conn.put_status(401)
        |> Phoenix.Controller.json(%{error: "unauthorized"})
    end
end