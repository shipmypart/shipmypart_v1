defmodule Shipmypart.Auth do
    import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]

  defp hash(password) do
    :crypto.hash(:sha512, password) |> Base.encode64
  end

    def basic_login(email, given_pass, opts) do
        repo = Keyword.fetch!(opts, :repo)
        user = repo.get_by(Shipmypart.User, %{email: email})
        cond do
            user && !user.is_active ->
                {:error, :unprocessable_entity}
            user && checkpw(hash(given_pass), user.encrypted_password) ->
                {:ok, user}
            user ->
                {:error, :unauthorized}
            true ->
                dummy_checkpw()
                {:error, :not_found}
        end
    end
end
