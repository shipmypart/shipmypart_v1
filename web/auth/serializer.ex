defmodule Shipmypart.GuardianSerializer do
  @behaviour Guardian.Serializer

  alias Shipmypart.Repo
  alias Shipmypart.User

  def for_token(user = %User{}), do: { :ok, "U:#{user.id}" }
  def for_token(_), do: { :error, "Unknown resource type" }

  def from_token("U:" <> id), do: { :ok, (Repo.get(User, id) |> Repo.preload([:company])) }
  def from_token(_), do: { :error, "Unknown resource type" }
end