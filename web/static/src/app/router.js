import _ from 'lodash'
import localForage from 'localforage'
import Vue from 'vue'
import VueRouter from 'vue-router'

/* Non-standard */
import Main from './Main.vue'

/* Auth related */
import Login from './components/auth/Login.vue'
import Verify from './components/auth/Verify.vue'
import Reset from './components/auth/Reset.vue'
import Signup from './components/auth/Signup.vue'

/* components */
import Account from './components/account/Index.vue'
import Support from './components/support/Index.vue'
import Shipment from './components/shipment/Index.vue'
import NewShipment from './components/shipment/Create.vue'
import NotFound from './components/NotFound.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  saveScrollPosition: false,
  mode: 'hash',
  routes: [{
    path: '/',
    meta: {
      requiresAuth: true
    },
    component: Main,
    children: [{
      path: '',
      redirect: '/shipments'
    },
    {
      path: '/shipments',
      components: {
        content: Shipment
      }
    },
    {
      name: 'newShipment',
      path: '/shipments/:batchId/new',
      props: true,
      components: {
        content: NewShipment
      }
    },
    {
      path: '/accounts',
      components: {
        content: Account
      }
    },
    {
      path: '/support',
      components: {
        content: Support
      }
    }]
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      checkAuth: true
    }
  },
  {
    path: '/verify/:id',
    props: true,
    component: Verify,
    meta: {
      checkAuth: true
    }
  },
  {
    path: '/reset',
    component: Reset
  },
  {
    path: '/signup',
    name: 'signup',
    component: Signup,
    meta: {
      checkAuth: true
    }
  },
  {
    path: '/*',
    component: NotFound
  }]
})

function authCheck (to, from, next, token) {
  // Redirect if we require authentication
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!token) {
      return next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    }
    return next()
  }

  // Redirect if already authenticated
  if (to.matched.some(record => record.meta.checkAuth)) {
    if (token) {
      return next({
        path: '/shipments',
        query: {
          redirect: to.fullPath
        }
      })
    }
    return next()
  }

  return next()
}

router.beforeEach((to, from, next) => {
  const token = router.app.$store.state.auth.token
  if (!token) {
    return localForage.getItem('_STATE').then((value) => {
      return router.app.$store.replaceState(_.merge({}, router.app.$store.state, value))
    }).then(() => {
      return authCheck(to, from, next, router.app.$store.state.auth.token)
    })
  }
  return authCheck(to, from, next, token)
})

export default router
