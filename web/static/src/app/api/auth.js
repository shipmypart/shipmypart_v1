import axios from './index.js'

const apiPath = '/api/v1/auth/sessions'

export default {
  login (creds) {
    return axios.post(apiPath, creds)
  },
  logout () {
    return axios.delete(apiPath + '/_')
  }
}
