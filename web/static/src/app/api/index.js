import store from '../store'
import axios from 'axios'

const loginPath = '#/login'

// Create HTTP instance
const instance = axios.create()

// Intercept requests to append auth data
instance.interceptors.request.use(config => {
  if (store.state.auth.token) {
    config.headers.common['Authorization'] = 'Bearer ' + store.state.auth.token
  }
  return Promise.resolve(config)
}, error => {
  if (error) {
    return Promise.reject(error)
  }
})

// Intercept responses to determine if user is still logged instance
instance.interceptors.response.use(response => {
  return response
}, error => {
  if (error.response.status === 401) {
    return new Promise((resolve, reject) => {
      store.state.auth.token = ''
      if (window.location.hash.indexOf(loginPath) === -1) {
        window.location.hash = loginPath
      }
      return reject(error)
    })
  }
  return Promise.reject(error)
})

export default instance
