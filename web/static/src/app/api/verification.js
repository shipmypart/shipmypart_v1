import axios from './index.js'

const apiPath = '/api/v1/auth/activations'

export default {
  retrieve (id) {
    return axios.get([apiPath, id].join('/'))
  },
  verify (id, params) {
    return axios.put([apiPath, id].join('/'), params)
  }
}
