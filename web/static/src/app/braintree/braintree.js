import { client, hostedFields } from "braintree-web";

//var form = document.querySelector('#cardForm');
//var authorization = 'sandbox_g42y39zw_348pk9cgf3bgyw2b';

export default function createClient(form) {
    function createHostedFields(clientInstance) {
        hostedFields.create({
            client: clientInstance,
            styles: {
                'input': {
                    'font-size': '16px',
                    'font-family': 'courier, monospace',
                    'font-weight': 'lighter',
                    'color': '#ccc'
                },
                ':focus': {
                    'color': 'black'
                },
                '.valid': {
                    'color': '#8bdda8'
                }
            },
            fields: {
                number: {
                    selector: '#card-number',
                    placeholder: '4111 1111 1111 1111'
                },
                cvv: {
                    selector: '#cvv',
                    placeholder: '123'
                },
                expirationDate: {
                    selector: '#expiration-date',
                    placeholder: 'MM/YYYY'
                },
                postalCode: {
                    selector: '#postal-code',
                    placeholder: '11111'
                }
            }
        }, function (err, hostedFieldsInstance) {
            var teardown = function (event) {
                event.preventDefault();
                hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
                    if (tokenizeErr) {
                        console.error(tokenizeErr);
                        return;
                    }

                    // If this was a real integration, this is where you would
                    // send the nonce to your server.
                    alert('Got a nonce: ' + payload.nonce);
                    hostedFieldsInstance.teardown(function () {
                        createHostedFields(clientInstance);
                        form.removeEventListener('submit', teardown, false);
                    });
                });
            };

            form.addEventListener('submit', teardown, false);
        });
    }

    $.get("api/v1/paymenttokens/new", function (authorization) {
        client.create({
            authorization: authorization.payment_token
        }, function (err, clientInstance) {
            if (err) {
                console.error(err);
                return;
            }
            createHostedFields(clientInstance);
        });
    });
}
