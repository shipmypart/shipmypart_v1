import Vue from 'vue'
import Vuex from 'vuex'

/* plugins */
import storage from './plugins/storage'

import auth from './modules/auth'
import batch from './modules/batch'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    auth,
    batch
  },
  plugins: [storage()]
})

export default store
