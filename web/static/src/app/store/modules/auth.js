import moment from 'moment-timezone'
import auth from '../../api/auth'

export default {
  namespace: true,
  model: {
    user: {},
    token: '',
    expires: Date.now() - 1
  },
  actions: {
    LOGIN: ({ commit }, credentials) => {
      return auth.login(credentials).then(result => {
        commit('save', result.data)
        return result
      })
    },
    LOGOUT: ({ commit }) => {
      return new Promise((resolve, reject) => {
        return resolve(commit('destroy'))
      })
    }
  },
  mutations: {
    save (state, payload) {
      state.user = payload.user
      state.token = payload.token
      state.expires = payload.expires
    },
    destroy (state) {
      state.user = {}
      state.token = ''
      state.expires = Date.now() - 1
    }
  },
  getters: {
    isAuthenticated: state => {
      return state.token ? moment.unix(state.expires).isSameOrAfter(moment.utc()) : false
    },
    user: state => {
      return state.user
    }
  }
}
