import api from '../../api'

export default {
  namespace: true,
  state: {
    batches: []
  },
  actions: {
    LOAD_BATCHES ({ commit }) {
      return api.get('/api/v1/batches')
      .then((res) => {
        return commit('SET_BATCHES', { list: res.data })
      }, (err) => {
        if (err) {
          console.error(err)
        }
      })
    },
    NEW_BATCH ({ commit }) {
      return api.post('/api/v1/batches', {
        batch: {
          name: Date.now().toString().substring(3, 11)
        }
      })
      .then((res) => {
        return commit('ADD_BATCH', { batch: res.data })
      }, (err) => {
        if (err) {
          console.error(err)
        }
      })
    },
    PURCHASE_BATCH ({ commit }, batch) {
    },
    VOID_BATCH ({ commit }, batch) {
      return api.delete('/api/v1/batches/' + batch.id)
      .then((res) => {
        return commit('REMOVE_BATCH', { batch })
      }, (err) => {
        if (err) {
          console.error(err)
        }
      })
    }
  },
  mutations: {
    SET_BATCHES (state, { list }) {
      state.batches = list.data
    },
    ADD_BATCH (state, { batch }) {
      state.batches.unshift(batch.data)
    },
    UPDATE_BATCH (state, { batch, remove }) {
      var _batch = batch.data
      const idx = state.batches.map(x => x.id).indexOf(_batch.id)
      return state.batches.splice(idx, 1, _batch)
    },
    REMOVE_BATCH (state, { batch }) {
      const idx = state.batches.map(x => x.id).indexOf(batch.id)
      return state.batches.splice(idx, 1)
    }
  },
  getters: {
    getBatches (state) {
      return state.batches
    }
  }
}
