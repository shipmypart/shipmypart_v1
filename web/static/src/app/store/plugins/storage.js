import _ from 'lodash'
import localForage from 'localforage'

const defaultReducer = (state, paths) => (
  paths.length === 0 ? state : paths.reduce((substate, path) => {
    _.set(substate, path, _.get(state, path))
    return substate
  }, {})
)

export default function storage ({
  key = '_STATE',
  paths = [],
  getState = key => localForage.getItem(key),
  setState = (key, state) => localForage.setItem(key, state),
  reducer = defaultReducer
} = {}) {
  return store => {
    return getState(key).then((value) => {
      return store.replaceState(
        _.merge({}, store.state, value)
      )
    }).then(() => {
      store.subscribe((mutation, state) => {
        return setState(key, reducer(state, paths))
      })
    })
  }
}
