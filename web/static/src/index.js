import 'phoenix_html'
import Vue from 'vue'
import api from './app/api'
import router from './app/router'
import store from './app/store'
Vue.prototype.$http = api

export default new Vue({
  el: '#root',
  store,
  router,
  render: h => h('router-view')
})
