defmodule Shipmypart.Option do
  use Shipmypart.Web, :model
  use Shipmypart.Schema

  schema "options" do
    field :timezone, :string
    field :label_type, :string

    # Relationships
    belongs_to :user, Shipmypart.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:timezone, :label_type])
    |> validate_required([])
  end

  def associate_user(struct, params \\ %{}, user) do
    struct
    |> changeset(params)
    |> put_assoc(:user, user)
  end
end
