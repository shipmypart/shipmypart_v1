defmodule Shipmypart.Chart do
  use Shipmypart.Web, :model
  use Shipmypart.Schema
  
  schema "charts" do
    field :zone1, {:array, :string}
    field :zone2, {:array, :string}
    field :zone3, {:array, :string}
    field :zone4, {:array, :string}
    field :zone5, {:array, :string}
    field :zone6, {:array, :string}
    field :zone7, {:array, :string}
    field :zone8, {:array, :string}
    field :zone9, {:array, :string}
    field :service, :string
    field :expires, Timex.Ecto.DateTime
    field :available, Timex.Ecto.DateTime

    # Relationships
    belongs_to :company, Shipmypart.Company
    
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:available, :expires, :service, :zone1, :zone2, :zone3, :zone4, :zone5, :zone6, :zone7, :zone8, :zone9])
    |> cast_assoc(:company)
    |> validate_required([:available, :expires, :service, :zone1, :zone2, :zone3, :zone4, :zone5, :zone6, :zone7, :zone8, :zone9])
  end

  def associate_company(struct, params \\ %{}, user) do
    struct
    |> changeset(params)
    |> put_assoc(:company, user.company)
  end
end
