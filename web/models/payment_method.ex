defmodule Shipmypart.PaymentMethod do
  use Shipmypart.Web, :model
  use Shipmypart.Schema

  schema "paymentmethods" do
    field :card_type, :string
    field :cardholder_name, :string
    field :customer_id, :string
    field :default, :boolean
    field :expiration_month, :string
    field :expiration_year, :string
    field :expired, :boolean
    field :image_url, :string
    field :last_4, :string
    field :token, :string
    field :unique_number_identifier, :string
    field :verifications, {:array, :map}

    # Relationships
    belongs_to :company, Shipmypart.Company

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:verifications, :card_type, :cardholder_name, :customer_id, :default, :expiration_month, :expiration_year, :expired, :image_url, :last_4, :token, :unique_number_identifier])
    |> cast_assoc(:company)
    |> validate_required([:verifications, :card_type, :customer_id, :default, :expiration_month, :expiration_year, :expired, :image_url, :last_4, :token, :unique_number_identifier])
  end

  def from_braintree(struct, params \\ %{}, user) do
    struct
    |> changeset(%{
      card_type: params.card_type,
      cardholder_name: params.cardholder_name,
      customer_id: params.customer_id,
      default: params.default,
      expiration_month: params.expiration_month,
      expiration_year: params.expiration_year,
      expired: params.expired,
      image_url: params.image_url,
      last_4: params.last_4,
      token: params.token,
      unique_number_identifier: params.unique_number_identifier,
      verifications: params.verifications
    })
    |> put_assoc(:company, user.company)
  end
end
