defmodule Shipmypart.Location do
  use Shipmypart.Web, :model
  use Shipmypart.Schema
  
  schema "locations" do
    field :name, :string

    # Relationships
    belongs_to :company, Shipmypart.Company

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
  end

  def associate_company(struct, params \\ %{}, user) do
    struct
    |> changeset(params)
    |> put_assoc(:company, user.company)
  end
end
