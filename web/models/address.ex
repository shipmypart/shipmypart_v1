defmodule Shipmypart.Address do
  use Shipmypart.Web, :model
  use Shipmypart.Schema

  schema "addresses" do
    field :name, :string
    field :company, :string
    field :street1, :string
    field :street2, :string
    field :city, :string
    field :state, :string
    field :zip, :string
    field :country, :string
    field :phone, :string
    field :email, :string
    field :easypost_id, :string

    # Relationships
    belongs_to :_company, Shipmypart.Company, foreign_key: :company_id
    has_many :shipments, Shipmypart.Shipment
    
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :company, :street1, :street2, :city, :state, :zip, :country, :phone, :email, :easypost_id])
    |> cast_assoc(:_company)
    |> validate_required([:street1, :city, :state, :zip, :country, :phone])
  end

  def associate_company(struct, params \\ %{}, user) do
    struct
    |> changeset(params)
    |> put_assoc(:_company, user.company)
  end
end
