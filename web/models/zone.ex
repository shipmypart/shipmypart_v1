defmodule Shipmypart.Zone do
  use Shipmypart.Web, :model

  schema "zones" do
      field :origin, :string
      field :dest, :string
      field :zone, :integer
      field :discounted, :boolean
      field :effective, Timex.Ecto.DateTime
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([])
    |> unique_constraint(:origin_dest)
  end
end
