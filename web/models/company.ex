defmodule Shipmypart.Company do
  use Shipmypart.Web, :model
  use Shipmypart.Schema

  schema "companies" do
    field :name, :string
    field :is_active, :boolean, default: false
    field :is_past_due, :boolean, default: false
    # field :subscription_amount, :float, default: 13.00

    # Relationship
    belongs_to :user, Shipmypart.User
    has_many :addresses, Shipmypart.Address
    has_many :batches, Shipmypart.Batch
    has_many :paymentmethods, Shipmypart.PaymentMethod
    has_many :charts, Shipmypart.Chart
    has_many :locations, Shipmypart.Location
    has_many :shipments, Shipmypart.Shipment
    has_many :transactions, Shipmypart.Transaction

    timestamps()
  end

  def current_balance(company) do
    Repo.one from(
      t in Shipmypart.Transaction,
      where: t.company_id == ^company.id,
      order_by: [desc: t.updated_at],
      limit: 1,
      select: t.balance
    )
  end

  def can_purchase(company, amount) do
    current_balance(company) >= amount
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :is_active, :is_past_due])
    |> validate_required([:name])
  end
end
