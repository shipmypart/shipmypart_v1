defmodule Shipmypart.Transaction do
  use Shipmypart.Web, :model
  use Shipmypart.Schema

  alias Shipmypart.Company
  
  schema "transactions" do
    field :description, :string
    field :credit, :float
    field :debit, :float
    field :balance, :float

    # Relationships
    belongs_to :company, Shipmypart.Company

    timestamps()
  end

  def new(company) do
      changeset(%__MODULE__{}, %{
        description: "[INIT] Creation for: " <> company.name,
        credit: 0.0,
        balance: 0.0
      })
      |> put_assoc(:company, company)
      |> Repo.insert!
  end

  def add(company, params \\ %{}) do
      changeset(%__MODULE__{}, %{
        description: params.description,
        credit: params.amount
      })
      |> put_change(:balance, Company.current_balance(company) + (Float.parse(params.amount) |> elem(0)))
      |> put_assoc(:company, company)
      |> Repo.insert!
  end

  def subtract(company, params \\ %{}) do
      changeset(%__MODULE__{}, %{
        description: params.description,
        debit: params.amount
      })
      |> put_change(:balance, Company.current_balance(company) - (Float.parse(params.amount) |> elem(0)))
      |> put_assoc(:company, company)
      |> Repo.insert!
  end

  defp changeset(struct, params) do
    struct
    |> cast(params, [:description, :credit, :debit, :balance])
    |> cast_assoc(:company)
    |> validate_number(:balance, greater_than_or_equal_to: 0.0, message: "funds insufficient to complete this transaction")
  end
end
