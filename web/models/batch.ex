defmodule Shipmypart.Batch do
  use Shipmypart.Web, :model
  use Shipmypart.Schema
  
  schema "batches" do
    field :name, :string
    field :status, :string
    field :state, :string
    field :easypost_id, :string

    # Relationships
    has_many :shipments, Shipmypart.Shipment
    belongs_to :company, Shipmypart.Company

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :status, :state, :easypost_id])
    |> validate_required([])
  end

  def associate_company(struct, params \\ %{}, user) do
    struct
    |> changeset(params)
    |> put_assoc(:company, user.company)
  end
end
