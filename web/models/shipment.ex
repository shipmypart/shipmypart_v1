defmodule Shipmypart.Shipment do
  use Shipmypart.Web, :model
  use Shipmypart.Schema

  alias Shipmypart.Transaction
  
  schema "shipments" do
    field :parcel, :map
    field :customs_info, :map
    field :tracking_code, :string
    field :insurance, :float
    field :reference, :string
    field :carrier, :string
    field :service, :string
    field :rate, :float
    field :label_url, :string
    field :easypost_id, :string
    field :purchased, :boolean
    field :refunded, :boolean
    field :in_transit, :boolean
    field :delivered, :boolean
    field :rates, {:array, :map}, virtual: true

    # Relationships
    belongs_to :chart, Shipmypart.Chart
    belongs_to :batch, Shipmypart.Batch
    belongs_to :company, Shipmypart.Company
    belongs_to :to_address, Shipmypart.Address, foreign_key: :to_address_id
    belongs_to :transaction, Shipmypart.Transaction
    belongs_to :from_address, Shipmypart.Address, foreign_key: :from_address_id

    timestamps()
  end

  def easypost_purchase(current_user, id, purchased_shipment) do
    transaction = Transaction.subtract(current_user.company, %{
      description: "Purchase of shipment: " <> purchased_shipment.tracking_code,
      amount: purchased_shipment.selected_rate.rate
    })
    changeset(Repo.get!(__MODULE__, id) |> Repo.preload([:chart, :batch, :company, :to_address, :transaction, :from_address]), %{
      "purchased" => true,
      "tracking_code" => purchased_shipment.tracking_code,
      "carrier" => purchased_shipment.selected_rate.carrier,
      "rate" => purchased_shipment.selected_rate.rate,
      "service" => purchased_shipment.selected_rate.service,
      "label_url" => purchased_shipment.postage_label.label_url
    })
    |> put_assoc(:transaction, transaction)
    |> Repo.update
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:rates, :delivered,
                     :parcel, :customs_info, 
                     :tracking_code, :insurance, 
                     :reference, :carrier, 
                     :service, :rate, 
                     :label_url, :easypost_id,
                     :purchased, :refunded,
                     :in_transit])
    # |> cast_assoc(:chart)
    # |> cast_assoc(:batch)
    # |> cast_assoc(:company)
    # |> cast_assoc(:to_address)
    # |> cast_assoc(:transaction)
    # |> cast_assoc(:from_address)
    |> validate_required([:parcel, :easypost_id])
  end
end
