defmodule Shipmypart.User do
  use Shipmypart.Web, :model
  use Shipmypart.Schema

  alias Braintree.Customer
  
  schema "users" do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :encrypted_password, :string
    field :password, :string, virtual: true
    field :is_active, :boolean, default: false
    field :activation_token, :string
    field :activation_token_expiration, Timex.Ecto.DateTime
    field :admin, :boolean, default: false
    field :customer_id, :string

    # Relationships
    has_one :company, Shipmypart.Company

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:first_name, :last_name, :email, :password])
    |> validate_required([:first_name, :last_name, :email])
    |> unique_constraint(:email)
    |> validate_format(:email, ~r/@/iu)
    |> validate_length(:password, min: 8)
    |> hash_password
  end

  def activation_changeset(struct, params \\ %{}) do
    struct
    |> changeset(params)
    |> validate_required([:password])
    |> put_change(:is_active, true)
    |> remove_confirmation_token
    |> should_be_admin
    |> create_customer_id
  end

  def registration_changeset(struct, params \\ %{}) do
    struct
    |> changeset(params)
    |> cast_assoc(:company)
    |> put_change(:encrypted_password, "TEMP")
    |> validate_required([:company])
    |> generate_confirmation_token
  end

  # Private Methods
  defp hash_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :encrypted_password, hash(pass))
      _ ->
        changeset
    end
  end

  defp hash(password) do
    :crypto.hash(:sha512, password) |> Base.encode64 |> Comeonin.Bcrypt.hashpwsalt
  end

    # Private Methods
  defp create_customer_id(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{first_name: first_name, last_name: last_name, email: email}} ->
        case Customer.create(%{
          first_name: first_name,
          last_name: last_name,
          email: email
        }) do
          {:ok, customer} ->
            changeset
            |> put_change(:customer_id, customer.id)
          {:error, errors} ->
            IO.puts errors
            changeset
        end
      _ ->
        changeset
    end
  end

  defp generate_confirmation_token(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true} ->
        changeset
        |> put_change(:activation_token, create_random_string(32))
        |> put_change(:activation_token_expiration, create_expiration_date())
      _ ->
        changeset
    end
  end

  defp remove_confirmation_token(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true} ->
        changeset
        |> put_change(:activation_token, "")
        |> put_change(:activation_token_expiration, Timex.now)
    end
  end

  defp should_be_admin(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{email: email}} ->
        cond do
          Regex.match?(~r/@shipmypart.com$/iu, email) ->
            put_change(changeset, :admin, true)
          true ->
            changeset
        end
      _ ->
        changeset    
    end
  end

  defp create_random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
  end

  defp create_expiration_date() do
    Timex.now
    |> Timex.shift(days: 7)
  end
end
