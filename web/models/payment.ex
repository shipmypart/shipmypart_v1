defmodule Shipmypart.Payment do
  use Shipmypart.Web, :model
  use Shipmypart.Schema
  
  schema "payments" do

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([])
  end
end
