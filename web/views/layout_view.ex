defmodule Shipmypart.LayoutView do
  use Shipmypart.Web, :view

  def analytics_token() do
    Application.get_env(:shipmypart, :analytics_key)
  end
end
