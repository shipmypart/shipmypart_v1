defmodule Shipmypart.V1.PaymentView do
  use Shipmypart.Web, :view

  def render("index.json", %{payments: payments}) do
    %{data: render_many(payments, Shipmypart.V1.PaymentView, "payment.json")}
  end

  def render("show.json", %{payment: payment}) do
    %{data: render_one(payment, Shipmypart.V1.PaymentView, "payment.json")}
  end

  def render("payment.json", %{payment: payment}) do
    %{id: payment.id}
  end
end
