defmodule Shipmypart.V1.AddressView do
  use Shipmypart.Web, :view

  def render("index.json", %{addresses: addresses}) do
    %{data: render_many(addresses, Shipmypart.V1.AddressView, "address.json")}
  end

  def render("show.json", %{address: address}) do
    %{data: render_one(address, Shipmypart.V1.AddressView, "address.json")}
  end

  def render("address.json", %{address: address}) do
    %{id: address.id,
      alt_id: address.easypost_id,
      name: address.name,
      company: address.company,
      street1: address.street1,
      street2: address.street2,
      city: address.city,
      zip: address.zip,
      country: address.country,
      phone: address.phone,
      email: address.email
    }
  end
end
