defmodule Shipmypart.V1.Admin.ChartView do
  use Shipmypart.Web, :view

  def render("index.json", %{charts: charts}) do
    %{data: render_many(charts, Shipmypart.V1.Admin.ChartView, "chart.json")}
  end

  def render("show.json", %{chart: chart}) do
    %{data: render_one(chart, Shipmypart.V1.Admin.ChartView, "chart.json")}
  end

  def render("chart.json", %{chart: chart}) do
    %{id: chart.id}
  end
end
