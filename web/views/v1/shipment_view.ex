defmodule Shipmypart.V1.ShipmentView do
  use Shipmypart.Web, :view

  def render("index.json", %{shipments: shipments}) do
    %{data: render_many(shipments, Shipmypart.V1.ShipmentView, "shipment.json")}
  end

  def render("show.json", %{shipment: shipment}) do
    %{data: render_one(shipment, Shipmypart.V1.ShipmentView, "shipment.json")}
  end

  def render("shipment.json", %{shipment: shipment}) do
    %{id: shipment.id,
      shipment: %{
        metadata: %{
          reference: shipment.reference,
          purchased: shipment.purchased,
          refunded: shipment.refunded,
          in_transit: shipment.in_transit,
          delivered: shipment.delivered,
          carrier: shipment.carrier,
          service: shipment.service,
          rate: shipment.rate
        },
        from_address: %{
          # id: shipment.from_address.id,
          name: shipment.from_address.name,
          company: shipment.from_address.company,
          street1: shipment.from_address.street1,
          street2: shipment.from_address.street2,
          city: shipment.from_address.city,
          state: shipment.from_address.state,
          zip: shipment.from_address.zip,
          country: shipment.from_address.country,
          phone: shipment.from_address.phone,
          email: shipment.from_address.email
          # easypost_id: shipment.from_address.easypost_id
        },
        to_address: %{
          # id: shipment.to_address.id,
          name: shipment.to_address.name,
          company: shipment.to_address.company,
          street1: shipment.to_address.street1,
          street2: shipment.to_address.street2,
          city: shipment.to_address.city,
          state: shipment.to_address.state,
          zip: shipment.to_address.zip,
          country: shipment.to_address.country,
          phone: shipment.to_address.phone,
          email: shipment.to_address.email
          # easypost_id: shipment.to_address.easypost_id
        },

        parcel: shipment.parcel,
        rates: shipment.rates,
      }
    }
  end
end
