defmodule Shipmypart.V1.BatchView do
  use Shipmypart.Web, :view

  def render("index.json", %{batches: batches}) do
    %{data: render_many(batches, Shipmypart.V1.BatchView, "batch.json")}
  end

  def render("show.json", %{batch: batch}) do
    %{data: render_one(batch, Shipmypart.V1.BatchView, "batch.json")}
  end

  def render("batch.json", %{batch: batch}) do
    %{id: batch.id,
      name: batch.name,
      status: batch.status,
      state: batch.state,
      alt_id: batch.easypost_id,
      shipments: batch.shipments,
      created: batch.inserted_at |> Timex.to_unix}
  end
end
