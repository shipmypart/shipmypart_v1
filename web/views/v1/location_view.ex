defmodule Shipmypart.V1.LocationView do
  use Shipmypart.Web, :view

  def render("index.json", %{locations: locations}) do
    %{data: render_many(locations, Shipmypart.V1.LocationView, "location.json")}
  end

  def render("show.json", %{location: location}) do
    %{data: render_one(location, Shipmypart.V1.LocationView, "location.json")}
  end

  def render("location.json", %{location: location}) do
    %{id: location.id}
  end
end
