defmodule Shipmypart.V1.PaymentTokenView do
  use Shipmypart.Web, :view

  def render("index.json", %{paymenttokens: paymenttokens}) do
    %{data: render_many(paymenttokens, Shipmypart.V1.PaymentTokenView, "payment_token.json")}
  end

  def render("show.json", %{payment_token: payment_token}) do
    %{data: render_one(payment_token, Shipmypart.V1.PaymentTokenView, "payment_token.json")}
  end

  def render("payment_token.json", %{payment_token: payment_token}) do
    %{payment_token: payment_token}
  end
end
