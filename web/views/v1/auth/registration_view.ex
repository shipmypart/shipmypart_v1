defmodule Shipmypart.V1.Auth.RegistrationView do
  use Shipmypart.Web, :view

  def auth_url(conn, user) do
    Enum.join([page_url(conn, :index) <> "#/verify", user |> create_activation_url], "/")
  end

  def render("show.json", %{registration: registration}) do
    %{data: render_one(registration, Shipmypart.V1.Auth.RegistrationView, "registration.json")}
  end

  def render("registration.json", %{registration: registration}) do
    %{id: registration.id,
      message: "created"}
  end

  defp create_activation_url(user) do
    user.activation_token
  end
end
