defmodule Shipmypart.V1.Auth.SessionView do
  use Shipmypart.Web, :view

  def render("error.json", %{message: message}) do
    %{message: message}
  end

  def render("show.json", %{session: session}) do
    render_one(session, Shipmypart.V1.Auth.SessionView, "session.json")
  end

  # return token
  def render("session.json", %{session: session}) do
    %{token: session.jwt,
      expires: session.exp,
      user: %{
        id: session.user.id,
        first_name: session.user.first_name,
        last_name: session.user.last_name,
        email: session.user.email,
      }
    }
  end
end
