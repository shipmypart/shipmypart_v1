defmodule Shipmypart.V1.Auth.ActivationView do
  use Shipmypart.Web, :view

  def render("show.json", %{activation: activation}) do
    %{data: render_one(activation, Shipmypart.V1.Auth.ActivationView, "activation.json")}
  end

  def render("activation.json", %{activation: activation}) do
    %{name: Enum.join([activation.first_name, activation.last_name], " "),
      email: activation.email}
  end

  def render("activation_error.json", %{error: error}) do
    %{ message: error.message }
  end
end
