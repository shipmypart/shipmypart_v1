defmodule Shipmypart.V1.PaymentMethodView do
  use Shipmypart.Web, :view

  def render("token.json", %{token: token}) do
    %{token: token}
  end

  def render("braintree_error.json", %{error: error}) do
    %{message: error.message,
      errors: error.errors}
  end

  def render("index.json", %{paymentmethods: paymentmethods}) do
    %{data: render_many(paymentmethods, Shipmypart.V1.PaymentMethodView, "payment_method.json")}
  end

  def render("show.json", %{payment_method: payment_method}) do
    %{data: render_one(payment_method, Shipmypart.V1.PaymentMethodView, "payment_method.json")}
  end

  def render("payment_method.json", %{payment_method: payment_method}) do
    %{id: payment_method.id}
  end
end
