defmodule Shipmypart.V1.OptionView do
  use Shipmypart.Web, :view

  def render("index.json", %{options: options}) do
    %{data: render_many(options, Shipmypart.V1.OptionView, "option.json")}
  end

  def render("show.json", %{option: option}) do
    %{data: render_one(option, Shipmypart.V1.OptionView, "option.json")}
  end

  def render("option.json", %{option: option}) do
    %{id: option.id}
  end
end
