defmodule Shipmypart.RateView do
  use Shipmypart.Web, :view

  def render("index.json", %{rates: rates}) do
    %{data: render_many(rates, Shipmypart.RateView, "rate.json")}
  end

  def render("show.json", %{rate: rate}) do
    %{data: render_one(rate, Shipmypart.RateView, "rate.json")}
  end

  def render("rate.json", %{rate: rate}) do
    %{id: rate.id}
  end
end
