defmodule Shipmypart.Router do
  use Shipmypart.Web, :router

  # Example Resources
  # user_path  GET    /users           UserController.index/2
  # user_path  GET    /users/:id/edit  UserController.edit/2
  # user_path  GET    /users/new       UserController.new/2
  # user_path  GET    /users/:id       UserController.show/2
  # user_path  POST   /users           UserController.create/2
  # user_path  PATCH  /users/:id       UserController.update/2
  #            PUT    /users/:id       UserController.update/2

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["urlencoded", "json"]
    plug :put_secure_browser_headers
  end

  pipeline :api_auth do    
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.EnsureAuthenticated, handler: Shipmypart.AuthHandler
    plug Guardian.Plug.LoadResource
    plug Shipmypart.CurrentUser
  end

  ### Endpoints only available during dev
  if Mix.env == :dev do
    forward "/sent_emails", Bamboo.EmailPreviewPlug
  end

  scope "/", Shipmypart do
    pipe_through :browser # Use the default browser stack
    resources "/", PageController, only: [:index]
    get "/.well-known/acme-challenge/:id", PageController, :letsencrypt
  end

  # Other scopes may use custom stacks.
  pipe_through [ :api ]
  scope "/api", Shipmypart do

    # API Routes
    scope "/v1", V1, as: :v1 do
      # Unauthenticated Resources
      resources "/paymenttokens", PaymentTokenController, only: [:new]
      resources "/rates", RateController, only: [:show]

      # Auth Resources (Unauthenticated)
      scope "/auth", Auth, as: :auth do
        resources "/activations", ActivationController, only: [:show, :update]
        resources "/registrations", RegistrationController, only: [:show, :create, :delete] 
        resources "/sessions", SessionController, only: [:create, :delete]
      end

      # Authenticated Resources
      pipe_through [ :api_auth ] # Enable Authentication
      resources "/addresses", AddressController, except: [:new, :edit]
      resources "/batches", BatchController, except: [:new, :edit]
      resources "/companies", CompanyController, except: [:new, :create, :edit]
      resources "/locations", LocationController, except: [:new, :edit]
      resources "/options", OptionController, except: [:new, :edit]
      resources "/payments", PaymentController, except: [:new, :edit]
      resources "/paymentmethods", PaymentMethodController, except: [:new, :edit, :update]
      resources "/shipments", ShipmentController, except: [:new, :edit]

      # Administrative Resources (Authenticated)
      scope "/admin", Admin, as: :admin do
        resources "/charts", ChartController, except: [:new, :edit]
      end
    end
  end
end
